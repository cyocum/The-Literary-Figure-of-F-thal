\chapter{Edition of a Dialogue between Cormac and F\'{i}thal}

As discussed briefly in the preceding chapters, the place in which \F{} changes from being an obscure legal and wisdom figure to a more widely known character in Irish literature is the poem analysed in this chapter.  The poem is given its own chapter for two reasons: the need for a new edition which collates and analyses the poem in its own right; the important part that this poem plays in the evolution of the character of \F{}.  The goal of this chapter is to rectify the lack of a modern edition and to bring attention to this, the most remarkable in the corpus of \F{} references.  There are four manuscript witnesses to the \emph{Immacallam Cormaic ocus F\'{\i}thail} (hereafter \emph{Immacallam}), a title bestowed for convenience since none of the manuscript witnesses gives the poem a proper title.  These are: Trinity College Dublin (TCD) MS 1339 pg.\ 149 (hereafter A), Bodleian MS Rawlinson B512 f116v b (hereafter B), TCD MS 1337 pg.\ 40 b (hereafter C), and Bodleian MS.\ Ir.\ d.\ 5 (fragment) (hereafter F).

The poem consists of a dialogue between the mythical King of Ireland, Cormac mac Airt,\footcite{OCathasaigh1977} and the equally mythical judge, F\'{\i}thal.  The metre of the poem is \emph{rannaigecht}\index{rannaigecht@\emph{rannaigecht}} with a shortening of the number of syllables in the first line, which is common in poetic dialogues.\footcite[13--15]{Knott1957}  The poem contains nine stanzas and a verse spoken by F\'{\i}thal begins and ends the poem.  A title is prefixed to C; however, this title has much in common with the prologue to the \emph{Immacallam}, which is in B, and is hardly a proper title to the poem itself.

The poem consists of an argument in relation to a feast that Cormac seems to have held without inviting F\'{\i}thal.  The prologue and title attached to B and C respectively attempt to explain the background to the poem by describing the circumstances that gave rise to the nine original stanzas.  The prologue describes a `substantial little feast' held by Cormac at Tara without inviting F\'{\i}thal.  Taking this as an insult, F\'{\i}thal confronts Cormac on the following day and complains about not having been invited.  After the explanation of the circumstances, the prologue in MS B interjects two additional stanzas to the poem before giving the original poem.  The poem proper presents the argument in a deliberately obscure way, which nevertheless reveals the status of F\'{\i}thal and Cormac as contrasting wisdom figures within the conventions of the Irish tradition.  F\'{\i}thal threatens to leave Cormac over the slight, while Cormac attempts to minimise the damage done to his relationship with F\'{\i}thal.  Later, the poets of the Classical Gaelic era used the \emph{Immacallam} as an \emph{exemplum} of the moody character of poets, which great kings must endure (see Section~\ref{sec:AChormaic}).

The poem is important because it illuminates at three levels.  First, it could be supposed that the poem was originally composed as an exploration of the relationship of a king to his judge.  This relationship was always potentially contentious, because of the tension between a tradition bearer such as a judge and a person making executive decisions about the daily management of a community.\footnote{\cite[45--52]{Gerriets1988}.  See also \cite[23--25]{Kelly1988}.}  The delicate balance of interests involved could easily cause misunderstandings of several kinds.  Second, this poetic conflict provided a source for later literary productions concerning kingship and its relationship with other sources of traditional wisdom.  Third,  this poem brings \F{} out of his relatively obscure place in the mythological fringes of early Irish law and places him in a more accessible literary setting, which poets and other authors of early Irish literature could, and did, develop further.

By the end of the poem, Cormac and F\'{\i}thal have not come to a proper resolution of their disagreement.  In the course of the poem, Cormac emphasises that he has shown the proper love for his \emph{amus}\index{amus@\emph{amus}} `hireling' (a reference to the fact that judges were considered part of the \emph{\'{a}es d\'{a}na}\index{aes dana@\emph{\'{a}es d\'{a}na}}\footcite[51--56]{Kelly1988}), and that he is unwilling to be without F\'{\i}thal in his role as judge.  Significantly, F\'{\i}thal is given the last stanza. The phrase which opens the poem, \emph{n\'{\i} ba m\'{e}}, is repeated twice there, once at the beginning and once at the end.\footcite[13]{Knott1957}  While it is very common in Irish syllabic poetry to end the poem with the first line, repeating it within the last stanza accentuates F\'{i}thal's anger at Cormac's slight to his honour and shows that he is still resentful.  The outcome of this argument is not explained in any of the MSS, and in literary terms the outcome of the \emph{Immacallam} seems moot.  No other stories which feature F\'{i}thal allude to the \emph{Immacallam} and what lasting effect it might have had upon his relationship with Cormac.

\section{Discussion of Manuscript Witnesses}

The several manuscripts preserve \emph{Immacallam} in various states of completeness.  A, B, and C have complete copies of the poem.  F is a fragmentary version of the poem closely following A, but it does have one interesting textual variation which is discussed in the section on the reconstructed text.  Linguistically, A's is the oldest version of the poem, while B and C are slightly later.  Textually, A stands a little apart from B and C, which are closely related to one another, as is shown in the following discussion.

\subsection{`A': TCD MS 1339 (The Book of Leinster)}

Our poem appears on page 149 of TCD MS 1339, which is also known as `The Book of Leinster'\index{Book of Leinster}.  The poem begins 10.5 cm from the top left of the verso page.  The binding is clearly in poor condition and the text itself is damaged by a tear in the manuscript which begins at 18.4 cm and ends, when it joins a hole, at 20.1 cm from the top left corner.  This hole is likely have resulted from the tanning process, since the scribe clearly wrote the text of the poem around the hole.  The damage from the tear occurred after the text was written, but affects only a part of two lines.  More frustrating to the transcriber is a stain that covers the entire left-hand column.  The stain becomes darker as it extends down the column, and is especially dark near the hole.  The opaqueness of the stain in this area nearly obscures the last two stanzas of the poem on the left-hand side near the binding.

The transcription presented in this edition has been checked against Bergin and Best's diplomatic edition of the Book of Leinster\index{Book of Leinster}.\footnote{\emph{The Book of Leinster, formerly Lebar na N\'{u}achongb\'{a}la}, Best, R. I., O. Bergin, M. A. O'Brien, and A. O'Sullivan, eds., vol. 3 (Dublin: Dublin Institute for Advanced Studies, 1954--1983), pp. 623--4.}  It is of the highest quality and no errors were detected by the electronic digital scan of the manuscript that we conducted.  As a result, this transcription is found to be identical to the transcription provided by us here in Appendix A.

\subsection{`B': Rawl B512 116v b}

There are two parts to the text as found in Rawl.\ B512.\footnote{For a more detailed description of the manuscript see \cite[223-254]{OCuiv2001}.}  The first part is the prologue, which begins at 6.5 cm in the right-hand column and ends at 16.5 cm in the same column.  The body of the poem begins at 16.5 cm and ends at 25.5 cm in the right-hand column.  The binding is in fair to poor condition and there are stains around the outer edge of the manuscript.  There is also a stain which extends through the lower half of the prologue and through the entire text of the poem.  Despite these imperfections, the text is extremely clear and written in a bold hand.

A transcription, with translation, of the prologue and a transcription, without translation, of the poem are provided by Kuno Meyer\footnote{\emph{Hibernica Minora}. Being a fragment of an Old-Irish treatise on the Psalter, with translation, notes and glossary, and an Appendix containing extracts hitherto unpublished from MS. Rawlinson, B. 512 in the Bodleian Library. Edited by Kuno Meyer. With a facsimile. Anecdota Oxoniensia. Oxford, at the Clarendon Press.} in the Appendix of \emph{Hibernica Minora}, pp. 82--3.  We checked both the prologue and the poem against the manuscript and certain alterations to Meyer's transcription were warranted; a new transcription is found in Appendix A with textual variants incorporated into the \emph{apparatus criticus} of the edition.

\subsection{`C': TCD 1337 (H.3.18) p.\ 40 b}

MS TCD 1337 is divided into three sections, and each section is bound separately.  The page on which the poem appears is the second.  The page itself contains three poems in the same hand.  The last of these poems is the one under discussion here.

The page is 12 cm wide by 18.5 cm high.  The top and bottom of the page are worn and cracked.  The page number is written in a hand other than that of the scribe of the poems between the line of words at the top of the page and the top of the right-hand column.  There is a stain starting 2.3 cm from the top and ending at 3.1 cm from the top.  The MS is also slightly folded in places; the fold may have been present at the time of writing, since the letters are missing the ink where the fold appears.  The title of our poem starts in the left-hand column but is justified with the rest of the column on the left, i.\ e.\ next to the binding.  The poem itself is 9.8 cm long and ends 4 cm from the bottom edge of the MS.  This blank space indicates that this is the last poem in the present section.  A new single-column poem begins on the next page.  The title of our poem is 05 cm from the beginning of the poem and .4 cm from the end of the previous poem.

There is a tear which has been stitched with thread 5 cm from the bottom edge of the MS and it runs for 1 cm on the right side of the MS.  This also affects the verso of the folio. The MS is also slightly cracked in several places, between 6.1 cm from the bottom and 5.3 cm from the bottom.  The lower right corner has been sheared off with a sharp implement.

The poem is written in a clear but slightly cramped hand.  Unlike the other MSS, the letters which indicate which stanzas of the poem are spoken by each character are written on the right-hand side rather than the left, and are nearly flush with the binding.  Each stanza begins with a capital letter which corresponds in size to the letter denoting the speaker.

On the whole, the text of our poem is unaffected by the defects of the MS.  However, the ink from the recto side of the page has begun to run through the page and is slightly visible on the verso side.

\subsection{`F': MS.\ Ir.\ d.\ 5.}

MS.\ Ir.\ d.\ 5.\ is actually two fragments (hereafter fragment A and fragment B).\footcite[21]{OCuiv2001}   The identification of our poem in this MS was made by Brian \'{O} Cuiv.\footcite[21--23]{OCuiv2001}  MS.\ Ir.\ d.\ 5 survives in the form of two narrow vertical strips, comprising all that remains of a large folium from which at least three such strips have been cut.  The text of the folium was written, as often, in two columns.  On the recto side (where our poem was written), the vertical cuts were made (1) near the right-hand side of col.\ 1, (2) near the left-hand side of col. 2, and (3) near the right-hand side of col.\ 2.  Fragment A is the strip between cuts (1) and (2), the strip between cuts (2) and (3) is lost, and Fragment B is the strip between cut (3) and the recto right-hand margin. (There may have been another cut or cuts to the left of cut (1), but since all text to the left of cut (1) is lost, this question does not concern us.)  Our poem was written in the lower portion of col.\ 1 and the upper portion of col.\ 2.  Accordingly, Fragment A contains (lower left) the right-hand extremities of the lines containing the earlier stanzas, and also (upper right) the left-hand extremities of the lines containing the later stanzas, while Fragment B contains (upper left) the right-hand extremities of the lines containing the later stanzas of our poem.  Spaces before the beginning of our poem and after its end help to confirm that this placement of the surviving fragment is the right one, and that the overall dimensions of F corresponded closely to the versions found in other MSS.

The hand of the scribe of this manuscript is bold and clear.  Other than the fact that this manuscript is fragmentary, there appear to be no other physical defects or stains.  The manuscript is still in excellent condition and shows no signs of wear.  The cuts made to the manuscript to create the fragments were made with a knife or other sharp and easily handled implement and did not cause any other damage to the fragment.  \'{O} Cuiv suggests very plausibly that this was done for the purpose of using the fragments as binding support for a book.\footcite[21]{OCuiv2001}

\section{The Prologue}

The prologue of the poem in B and the superscription in C are closely similar in content.  The latter is, in effect, a one-sentence summary of the former.  Is B an expansion of C or is C an abbreviation of B?  There is not enough evidence to come to a firm conclusion.  Although there are suggestive similarities, in content and provenance, between Rawlinson B512 and TCD 1337, the composite nature of both MSS complicates the question of identifying possible direct connections between B and C.  The versions of the actual poem in B and C are very close to one another (e.\ g.\ they agree, against A, in the order of stanzas 2 and 4), but the minor variation between them is sufficient to indicate that neither is directly dependent on the other.  In terms of the development of the \emph{Immacallam} we are inclined to view the longer version of the preliminary matter in B as an expansion of the shorter version in C rather than the other way around; but this is, in the last resort, a subjective judgement.

The preliminary matter in B and C may have been generated by the line in stanza four (= stanza 2 in A), where Cormac talks about drinking and entertainments.  Yet, in the poem itself F\'{i}thal seems to be angrier about Cormac spending time with others than with him.  This irritation is predicated on the legal axiom that a king should never be without his judge in attendance, even during the labour-intensive sowing season.\footcite[52]{Kelly1988}  In this sense, Cormac has broken the bond of a king to his judge.  It is this sense of broken bond which drives the poem, and is implicit in the prologue.

The prologue attached to B can be broken into two parts.  The first is the prose introduction, of a sort which is commonly added to traditional material in the later stages of the transmission of poetical material.\footcite[150--154]{Flower1916}  The second part comprises two stanzas of poetry in the same style and metre as the original \emph{Immacallam}.  The prologue is separated from the \emph{Immacallam} by the line `Conid\emph{h} and do-r\'{o}nsat na rvnna,'  which clearly demarcates the beginning of the \emph{Immacallam} proper from the foregoing material.

In distinguishing added material from original material, the conservatism of the scribe comes to the fore.  While the prologue provides the context, or \emph{senchus}\index{senchus@\emph{senchus}}, of the poem, his use of \emph{na runna}, with the definite article, approximates to `the \emph{famous} verses', and places them in a separate dimension from what has gone before.  The substance of this material is protected by its traditional status, like holy writ.  This was perfectly compatible with casual, small-scale textual innovation, which could, and did, take place in this poem, as elsewhere in early Irish literature.  It may well be that orality, either directly, in the transmission of the \emph{Immacallam}, or indirectly, in the development of an attitude to transmitted text in general, played a part in shaping the prologue as we find it.

\section{Features of the Poem}

The text of MS A is the oldest of the four versions and is linguistically and orthographically the earliest.  It contains very little that can be considered out of the ordinary in a Middle Irish text.  As indicated above, the relationship of B and C is complicated by the question of the prologue; however, the text of the poem in B and C is fairly consistent in relation to spelling, both in adopting newer spellings and in retaining older spellings.

The main divergence between A and BC lies in the switching and replacing of lines in stanzas 2 and 4.  As regards B and C, either could be indebted to the other indirectly, even if it is unlikely that B is a copy of C or vice versa.  If one of B and C derives from A, the combination of overall closeness with the transposition in stanzas 2 and 4 could suggest a text remembered rather than copied.  The close equivalence of meaning of the `swapped' lines is certainly characteristic of what happened to orally transmitted texts.  Again, the other significant divergence is in the presentational matter of the `stage directions' which indicate who spoke each verse in the dialogue.  Had we been dealing with textual copying these visual signposts might have been transmitted more faithfully.  But those are mere straws in the wind.

F contains little information to add to our discussion of the MSS.  The first point to mention is found in stanza 4, line 4 of A and B, which reads: `b\'{i}d \'{i}tu ar n-\'{o}l a \.{F}ithail'.  In fragment A of F, the spelling `hitu' can be clearly seen in the upper right-hand corner.  The second item of interest is that whereas in A and B F\'{i}thal's and Cormac's names are abbreviated to a single letter followed by a period, the scribe of F partially writes out the names next to, and even encroaching on, the capital letter which begins each stanza of dialogue.  Otherwise, the text of F bears a very close resemblance to that of A.  Although, there is not enough text remaining in F to enable us to make a firm statement as to its closeness to A, the two MSS appear to be very nearly related.

\section{Dating the Poem}

Several features mark the prologue as later than the Old Irish period.\footcite[1--4]{McCone2005}  The first feature is the use of the independent pronoun, as in the prose part of the prologue `n\'{i} rucad d'\'{o}l na fleidhe h\'{e}', where the independent pronoun is the object of the verb \emph{rucad}, which would not have been usual in Old Irish.  The second is the use of the verbal particle `ro-' as an indicator of the simple past tense.  For example, in `F\'{i}thel ro-chan inso', the verbal particle `ro-' is prefixed to the preterite of the verb \emph{canaid}, `sings', as a marker of the past tense rather than as indicator of the perfect.  The third feature is the fossilisation \emph{at-bert}\index{at-bert@\emph{at-bert}}, the object pronoun (neuter) in the past tense of the verb \emph{as-beir}\index{as-beir@\emph{as-beir}} `says' in `at-bert F\'{\i}thel fris.'  On the other hand, the use of \emph{ro} instead of Early Modern \emph{do} in \emph{ro-chan} may be noted as a mark of the relative earliness.

As stated above, the prologue to the \emph{Immacallam} seeks to explain the circumstances which gave rise to the original poem.  Yet there is a slight difference of tone between the prologue and the \emph{Immacallam} itself.  The prologue shows an interpretation of the relationship between the two men in which Cormac reacts more strongly against F\'{i}thal's complaint.  This is especially clear in the stanza immediately prior to the beginning of the \emph{Immacallam}, where Cormac states that he is wiser than his father, that his rightness and sense are better, and that true judgement is better judgement.  This would seem to indicate that his judgement, being truer, is better than F\'{i}thal's judgement.  Cormac is thus presented in the prologue as more intolerant toward F\'{i}thal than in the poem itself.  Here, Cormac seems to be searching for a way to calm F\'{i}thal and to placate his justified anger at the breaking of the bond between judge and king, in-as-much as \F{} did not receive an invitation to the feast.

\section{Editorial Methodology}

The editorial approach adopted in this chapter is as follows.  The prologue in B is treated separately, and the summary in C is seen as supplementary to it.  This separation is expedient because, although the prologue is intimately connected to the poem, it is a separate composition from the original text of the poem.  As already noted, the author of the prologue sets his section aside from the main text of the poem by the line: `Conid\emph{h} and do r\'{o}nsat na rvnna' and in linguistic terms the poem is more consistently Middle Irish than the prologue, as will be shown.  The edition of the prologue is lightly normalised.

In this chapter, the poem is reconstructed with reference to all the extant witnesses, most notably A, B, and C.  F does not contain any readings which differ from A in a substantial way, apart from the reading `hitu' noted above.  As A is found in the earliest MS source and as a rule supplies the best readings by linguistic and metrical criteria, A is treated as the exemplar text.  The readings of B and C are presented in the \emph{apparatus}.  Diplomatic transcriptions of A, B, and C, including the prologue where available, are provided in the Appendix.  Missing length marks are indicated with a macron where language, metre, and manuscript evidence support it.

\section{The Prologue of \emph{Immacallam Cormaic ocus F\'{i}thail}}

Fithel rocan inso iar nol \.{f}leidi bici br\'{i}g\emph{h}mairi do cor\emph{m}ac sec\emph{h}a \textit{7} rofrecart corm\emph{a}c eisiv\emph{m} .i.\ fec\emph{h}t bai corm\emph{a}c ac \'{o}l fl\emph{eidi} b\emph{r}igm\emph{air}e i te\emph{m}r\emph{aig} b\'{a}i do\emph{no} fit\emph{h}el fe\'{i}gbriat\emph{hra}ch isi\emph{n} baili \textit{7} ni rvc\emph{ad} d\'{o}l na fleid\emph{h}e h\'{e}.  Dor\'{i}a\emph{cht} corm\emph{a}c arab\'{a}rach ina teg\emph{h} r\'{i}g \textit{7} atb\emph{er}t fith\emph{el} f\emph{ri}s. \'{O}l atibis sec\emph{h}am sa are\'{i}r a corm\emph{ac} ar f\'{i}th\emph{el}.  Is\emph{ed} \emph{ar} cor\emph{mac} noc\emph{h}a n\'{i}b\emph{ed} hath\emph{air} sec\emph{h} maitisi \emph{ar} fit\emph{h}el \emph{con}id de roc\emph{h}an fith\emph{el} \textit{7} ro freg\emph{air} corm\emph{ac}.


\begin{singlespace}
\settowidth{\versewidth}{Secha n\'{i} rachad d'\'{o}l.}
\begin{verse}[\versewidth]
Maitisi f\'{i}al fin\emph{n}gai\emph{n}e. \\
Br\emph{eth}em ro ba\'{i} ic \emph{ar}t ai\emph{n}f\emph{er}. \\
Sec\emph{h}a n\'{i} rach\emph{ad} d\'{o}l. \\
\emph{Ar} \'{o}r gall \textit{7} gaid\emph{el}.

Isa\emph{m} gait\emph{h}i i\emph{n}a \emph{ar}t. \\
Is\emph{ed} b\'{i}s mo sm\emph{acht} do \.{s}\'{i}r \\
is feir mo c\emph{er}t is mo c\'{i}all. \\
Is mo b\emph{er}im b\emph{re}th co f\'{i}r.
\end{verse}
\end{singlespace}

Conid\emph{h} and do r\'{o}\emph{n}sat na rvn\emph{n}a.

\section{Translation}

\F{} recited this after Cormac had consumed a small lively feast without him, and Cormac answered him, i.\ e.\ an occasion when Cormac was consuming a lively feast in Tara, [and] \F{} [the] keen-worded, was in the place, and he was not brought to consume the feast.  Cormac arrived in his king's house on the morrow and \F{} said to him: ``you had a feast without me last night, Cormac'', said \F{}. ``Aye,'' said Cormac.  ``Your father would not have done so without my foster-father,'' said \F{}.  So that is how \F{} recited and Cormac answered:

\begin{singlespace}
\settowidth{\versewidth}{It is it which is my power forever.}
\begin{verse}[\versewidth]
\begin{altverse}
\flagverse{\F{}}
My foster-father, noble Finngaine\index{Finngaine}, \\
He was the judge of Art A\'{i}nfer\index{Art Ainfer@Art A\'{i}nfer}; \\
He (= Art) would not go without him to drink \\
For the gold of foreigners and Gaels.
\end{altverse}

\begin{altverse}
\flagverse{Cormac}
I am wiser than Art\index{Art Ainfer@Art A\'{i}nfer}, \\
That is [the basis of] my rule is forever; \\
better is my right and my sense, \\
I give more judgements truly.
\end{altverse}
\end{verse}
\end{singlespace}

So it was then that they composed the [following] verses.

\section{Edition of \emph{Immacallam Cormaic ocus F\'{i}thail}}

\begin{singlespace}
\input{ednotes3.tex}
\end{singlespace}

\section{Translation}

\begin{singlespace}
\begin{linenumbers*}
\modulolinenumbers[5]
\settowidth{\versewidth}{who will take unrefined kings of this world}
\begin{verse}[\versewidth]
\begin{altverse}\flagverse{F\'{i}thal} It will not be me \\
who will cleave to someone beyond his time. \\
Bright is every new man, irascible is every tired man, \\
not identical is the treasure that every [man] finds.
\end{altverse}

\begin{altverse}\flagverse{Cormac} Oh F\'{i}thal \\
Ale is drunk, even after festivities. \\
It is no proper welcome (?) for guarantors to be disadvantaged; \\
\null [there are] knowledgeable men though it is \\
true that they are not F\'{i}thals.
\end{altverse}

\begin{altverse}\flagverse{F\'{i}thal} Oh Cormac \\
with [such] an amount of pride and fame; \\
the companionship of kings has divided us, \\
so that we are displeased at our humiliation.
\end{altverse}

\begin{altverse}\flagverse{Cormac} Oh F\'{i}thal \\
wait [a while], so that we are at peace. \\
There will be an ebb-tide on a great sea (i.\ e.\ after a flood-tide), \\
there will be thirst after drinking, oh F\'{i}thal.
\end{altverse}

\begin{altverse}\flagverse{F\'{i}thal} I am not myself a noble, \\
I shall say to you swiftly enough: \\
This is the worst that a warrior got \\
Being with a lord wise but deceitful.
\end{altverse}

\begin{altverse}\flagverse{Cormac} I will say it although it offends someone, \\
I will not hide [it as a stay] against our falling out: \\
the thing that worst suits a house  \\
\null [is] an unsuitable (?), reproachful servant.
\end{altverse}

\begin{altverse}\flagverse{F\'{i}thal} I will not give \\
love to anyone who does not love me. \\
I will not humble my importance \\
though I find myself far from my land.
\end{altverse}

\begin{altverse}\flagverse{Cormac} Gold [resides] with kings of great valour \\
from the beginning of the world; \\
I am reluctant to be without a servant \\
and I love (lit.\ have loved) my substance.
\end{altverse}

\begin{altverse}\flagverse{F\'{i}thal} It will not be me \\
whom they will take from my king of this life \\
after spurning of gold and horses; \\
though it be someone, it will not be me.
\end{altverse}
\end{verse}
\end{linenumbers*}
\end{singlespace}

\section{Notes}

\begin{description}
\item[br\'{i}gmaire] DIL D 190.43--45 translates this `after Cormac had enjoyed a convivial little drinking-feast.'
\item[atibis] 2 sing.\ preterite (originally perfective) of \emph{ibid} `drinks', with MI petrified object pronoun.
\item[2 linfes] 3rd per.\ rel.\ fut.\ of \emph{lenaid} `remains' see DIL L 99.7.\index{linfes@\emph{linfes}}
\item[4 hinund] from \emph{inunn}, \emph{innonn} `alike' see DIL I 294.17.\index{hinund@\emph{hinund}}
\item[7 dochor] `disadvantage, hurt' see DIL D 228.224.66.\index{dochor@\emph{dochor}}
\item[7 \'{i}ath] `land' is sometimes confused with \emph{f\'{i}ad}\index{fiad@\emph{f\'{i}ad}} `land' (see DIL 3 \emph{f\'{i}ad}\index{fiad@\emph{f\'{i}ad}} in MSS of later poetry, and it is possible that the poem originally had \emph{f\'{i}ad} \index{fiad@\emph{f\'{i}ad}} (see DIL 5 \emph{f\'{i}ad} \index{fiad@\emph{f\'{i}ad}} `honour, respect, reverence, esp.\ the honour bestowed on a guest according to his rank)'.
\item[8 f\={a}thaig] nom.\ plur.\ masc.\ from \emph{f\'{a}thach} `possessed of knowledge or skill' see DIL F 47.33.\index{fathaig@\emph{f\={a}thaig}}
\item[10 airdairc] (also \emph{airdirc}\index{airdirc@\emph{airdirc}} or \emph{ordairc}\index{ordairc@\emph{ordairc}}) needed for metre.\ `renowned, famous' see DIL A 186.79.
\item[11 c\={e}tlud r\'{i}g rodar n-inlaig] see DIL I 237.28--29.
\item[12 and 14 condar] cf.\ L. Breatnach, `An Mhe\'{a}n-Ghaeilge', p. 324 (312.194) for 1 pl.\ copula forms like this, which replaces OI \emph{condan}.\footcite[221--333]{LBreatnach1994}
\item[12 dimdaig] nom.\ plur.\ masc.\ from \emph{dimdach}\index{dimdach@\emph{dimdach}} `ungrateful, unsatisfied' see DIL D 215.119.19.\index{dimdaig@\emph{dimdaig}}
\item[12 tarbairt] see \emph{do-airbir}. The reading of BC is superior to that of A here.
\item[17 muad] A poetic word translated `noble', but it refers literally to external qualities of rank. It could also have the meaning `dejected', which could indicate a double meaning intended by the poet in this case.  See DIL M 177.44.\index{muad@\emph{muad}}
\item[22 \.{n}debech(?)] A's reading is difficult at this point, but B and C support it.  From \emph{deibech}\index{deibech@\emph{deibech}} (see also \emph{debach}\index{debach@\emph{debach}}) `strife, contention' see DIL D 200.1.30--38.\index{ndebech@\emph{\.{n}debech}}
\item[24 anaith (an\'{a}ith?)] see DIL A 323.5--9.
\item[27 -toirniub] see DIL \emph{do-airindi}.
\item[29 rogail] \emph{ro} + \emph{gal} (also \emph{gail}) `excess of valor' or `great valor'.  The particle \emph{ro} here is used as an intensifier.\index{rogail@\emph{rogail}}
\item[32 m'\.{f}olaig] for \emph{folaid} (?): see DIL F 280.59.

\end{description}

\section{Literary Analysis}

As intimated elsewhere in this thesis, this poem is extremely important for the development of \F{}.  This is the earliest source outside the legal literature where \F{} appears as a character in his own right.  In addition, this is also the first appearance of \F{} at his most active form as the angry and intransigent wisdom figure.  While recent theorising of the relationship of king to judge posits that the king was the ultimate arbiter of legal matters in early Ireland, this poem seems to indicate that the king was not a king without his chief judge.\footnote{See \cite{Gerriets1988}.}  On the other hand, it also demonstrates the strength of the relationship of Cormac and \F{}.

As this is the earliest instance of \F{} outside the law tracts, it is possible to argue that it was the pivotal point from which the figure of \F{} proceeded from the more esoteric legal material into the wider literary prose and poetic traditions.  This poem sets all the parameters for \F{}'s career in later literature: his connection with Cormac, the fact that he is a judge, the wisdom aspects of his character, and the fact that he is involved with, if not a practitioner himself (an attribute that would be added later), of poetry.  This would seem to indicate that all the later literature could have drawn general inspiration from this poem, or indeed that this poem directly or indirectly influenced the rest.

To judge from our understanding of the earlier legal literature involving \F{}, it would seem that this poem was made specifically to raise \F{}'s profile to a more exposed literary level.  The author would have needed to know \F{}'s character as a wisdom-figure through his appearances in early Irish law tracts.  The connection to Cormac mac Airt was casually accepted by later authors, especially in the Fenian\index{Fenian} tradition; but the origin of the connection is unclear.  The author may have chosen it because Cormac, as the wise king \emph{par excellence}, needed a wise judge \emph{par excellence} to enable him to fulfil all the requirements of a mythical king-hero.  Being such a figure, \F{} could provide the required complement to Cormac, at the same time adding an association with kingly wisdom and just ruling to his own `portfolio'.

For the poets of the Early Modern period, this poem would involve a further twist: the dramatic tension between poet and patron (see Section~\ref{sec:AChormaic}).  For the tradition embodied in the present poem was consistently interpreted by them as a prototypical instance of the unstable emotional state of poets, with the suggestion that conflict with their patrons was inherent in the poet-patron relationship.  The ideological point being made was, of course, that if the patrons were too hard on their poets they risked losing their support and thence also the praise which they needed to be kings.  This dramatic tension is also reflected in the Fenian\index{Fenian} material, especially in \emph{Duanaire Finn} XLVII, where Cormac encounters \F{} in a doublet of Cormac's own rise to power (see Section~\ref{sec:DFDissXLVII}).

\section{Conclusion}

This edition of the \emph{Immacallam Cormaic ocus F\'{\i}thail} has added to scholarly understanding of this interesting and unique Middle Irish poem and has demonstrated its importance in the literary `career' of \F{}.  That the poem was composed with deliberate intent to raise the profile of \F{} in the eyes of the early Irish literary establishment is highly probable.  How far this enterprise had to do with \F{} alone, and how far it was fuelled by interest in the complementary relationship between \F{} and Cormac, is still debatable; but the poem certainly had a large effect upon later literary output in relation to \F{} and Cormac.

This chapter represents the last of the textual evidence relating to \F{}.  The next chapter considers this evidence as a whole to demonstrate further the developing image of \F{} and to draw into consideration aspects of the evidence which could not be dealt with in the earlier chapters.

%%% Local Variables:
%%% TeX-master: "PhD.tex"
%%% End:
